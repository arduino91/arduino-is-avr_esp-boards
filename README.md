# arduino-is-avr_esp-boards

Zoznam nástrojov, balíkov a knižníc pre prácu s Arduino doskami a odvodenými PLC automatmi.


Obsahuje nástroj `arduino-cli` na kompiláciu a nahrávanie kódu z prostredia príkazového riadku GNU/linux-u.

## Postup:

1. Stiahnuť súbory zabalené pomocou *7z*:

  [link na úložisku MEGA](https://mega.nz/folder/RQEDnTpB#fIz15N5I_b1g5I1fS3tVeA)

   Na stiahnutie je potrebné označiť všetky súbory, a všetko sa po stlačení **Download** stiahne ako jeden *zip* súbor.
   
   
2. Rozbaliť *zip* súbor:

`unzip arduino-is,esp,rp-boards.zip`

3. Rozbaliť *7z* súbory:

`7z x arduino-is,esp,rp-boards/dot_arduino.7z.001`

(na toto je možné samozrejme použiť i grafické programy ako napr. *PeaZip*,…)

4. Vznikne (pozor skrytý!) adresár `.arduino15`, ktorý nakopírujeme do `~/`, napríklad pomocou *rsync*:

`rsync -hPur .arduino15 ~/`


No a teraz môžeme pracovať s *Arduino* doskami i bez prostredia *Arduino IDE*. 

